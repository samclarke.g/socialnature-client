angular.module('socialNatureApp')

	.controller('UserCtrl', function ($scope, $state, User, GithubService) {
	  "use strict";
	  var self = this;
	  self.user = User;

	  GithubService.get(self.user.gitHubId).then( function (result) {
        self.userRepos = result.data;
	  })
	  
	  self.updateUser = function () {
        self.user.$update();
	  };

	  self.deleteUser = function () {
	  	self.user.$delete().then( function () {
         $state.go('users', {}, { reload: true });
	  	});
	  }
	});

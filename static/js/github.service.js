angular.module('socialNatureApp')

  .service('GithubService', function ($http) {
    var GITHUB_URL = 'https://api.github.com/users/';
    return {
      get: function (Id) {
        return $http
        .get(GITHUB_URL + Id + '/repos')
        .success(function (result) {
          return result.data;
        })
        .error(function (result) {
          console.log('Service error: ', result);
        })
      },
    };
  })

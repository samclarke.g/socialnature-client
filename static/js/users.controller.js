angular.module('socialNatureApp')

	.controller('UsersCtrl', function ($scope, Users, UserService) {
	  "use strict";
	  var self = this;
	  self.users = Users;

	  self.newUser = new UserService();

	  self.saveNewUser = function () {
      self.newUser.$save(function () {
    	  // reset
        self.users = UserService.query();
        self.newUser = new UserService();
      });
	  };
	});

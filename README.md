# Notes

This is the sum of apporx. 3hrs of work. All basic requirements should be fulfilled but there are imporvments which would be made in all areas before this application is ready for launch. I have detailed some of the most obvious omissions/ improvements below.

# Configuration

The only application variable which may need to be set depending on the API location is the ROOT_URL variable in `js/github.service.js`. This is currently set to `http://localhost:1337` which corresponds to a typical endpoint for a Sails.js application in development.

# Further Work

1. Unit/ integration tests are needed.
2. No build pipeline -> in production static assets should be minified, root urls constructed programatically etc.
3. Design and styling can be improved, use of a style guide, import fonts, possibly use a CSS processor, media-queries for mobile devices etc.
4. Better handling iof errors, such as message flashing when API resources fail, logging, 500 and 404 pages etc.
5. application user accounts and authentication
angular.module('socialNatureApp')

  .service('UserService', function ($resource) {
    var ROOT_URL = 'http://localhost:1337';
    return $resource(ROOT_URL + '/user/:id', { id: '@id' }, {
      update: {
        method: 'PUT'
      }
    });
  })

angular.module('socialNatureApp', 
    [
     'ui.router',
     'ngResource',
     'ngAria',
     'ngMaterial',
    ]
)

.config(function ($stateProvider, $urlRouterProvider) {
  var PARTIALS_PATH = 'static/partials/';
  var ROOT_URL = 'http://localhost:1337';
  
  $urlRouterProvider.when('', '/');
  $urlRouterProvider.otherwise('/404/');
  
  $stateProvider
    .state('home', {
        url: '/',
        views: {
          'main': {
            controller: 'HomeCtrl',
            controllerAs: 'hm',
            templateUrl: PARTIALS_PATH + 'home.tmpl.html',
          },
        }
    })
    .state('users', {
        url: '/users/',
        views: {
          'main': {
            controller: 'UsersCtrl',
            controllerAs: 'usrs',
            templateUrl: PARTIALS_PATH + 'users.tmpl.html',
            resolve: {
              Users: function (UserService) {
                return UserService.query();
              },
            }
          },
        }
    })
    .state('user', {
        url: '/users/:id',
        views: {
          'main': {
            controller: 'UserCtrl',
            controllerAs: 'usr',
            templateUrl: PARTIALS_PATH + 'user.tmpl.html',
            resolve: {
              User: function (UserService, $stateParams) {
                return UserService.get({id: $stateParams.id}).$promise.then();
              },
            }
          },
        }
    })
});